﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public GameObject ball;
    public Transform target;
    public float ballForce;
    public int totalBalls;
    Plane plane = new Plane(Vector3.forward, 0);

    public Ball ballScript;
    public bool gameHasStarted;

    public int currentLevel;
    public GameObject[] barellSetGroup;

    public bool readyToShoot;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    void Start()
    {
        //   readyToShoot = true;
    }

    public void StartGame()
    {
        gameHasStarted = true;
        readyToShoot = true;
    }
    void Update()
    {

        Vector3 mousePos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 5));

        if (Input.GetMouseButton(0) && readyToShoot)
        {
            ball.GetComponent<Animator>().enabled = false;
            ball.transform.position = new Vector3(mousePos.x, ball.transform.position.y, ball.transform.position.z);
        }

        Vector3 dir = target.position - ball.transform.position;

        if (Input.GetMouseButtonUp(0) && readyToShoot)
        {
            ball.GetComponent<Rigidbody>().AddForce(dir * ballForce, ForceMode.Impulse);
            readyToShoot = false;
            totalBalls--;
            UIManager.instance.UpdateBallIcons();
            if (totalBalls <= 0)
            {
                print("Game Over");
            }
        }

        float dist;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (plane.Raycast(ray, out dist))
        {
            Vector3 point = ray.GetPoint(dist);
            target.position = new Vector3(point.x, point.y, 0);
        }
    }

    public void GroundFallenCheck()
    {
        if (AllGrounded())
        {
            print("Load new level");
            LoadNextLevel();
        }
    }

    public void LoadNextLevel()
    {
        if (gameHasStarted)
        {
            StartCoroutine(LoadNextLevelRoutine());
        }
    }

    IEnumerator LoadNextLevelRoutine()
    {
        Debug.Log("Loading Next Level...");
        yield return new WaitForSeconds(1.5f);
        UIManager.instance.showBlackFade();
        readyToShoot = false;
        barellSetGroup[currentLevel].SetActive(false);
        currentLevel++;

        if (currentLevel > barellSetGroup.Length) currentLevel = 0;
        yield return new WaitForSeconds(1.0f);
        barellSetGroup[currentLevel].SetActive(true);
        UIManager.instance.UpdateBallIcons();
        ballScript.RepositionBall();
    }

    bool AllGrounded()
    {
        Transform currentSet = barellSetGroup[currentLevel].transform;

        foreach(Transform t in currentSet)
        {
            if(t.GetComponent<Can>().hasFallen == false)
            {
                return false;
            }
        }
        return true;
    }
        
}
