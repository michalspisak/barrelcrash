﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{

    Vector3 spawnPos;

    void Start()
    {
        spawnPos = transform.position;
    }
    
    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("RestartPlane"))
        {
            RepositionBall();
        }
    }

    public void RepositionBall()
    {
        this.gameObject.SetActive(false); ;
        transform.position = spawnPos;
        this.GetComponent<Animator>().enabled = true;
        gameObject.SetActive(true);
        StartCoroutine(SetReadyToShoot());
    }

    IEnumerator SetReadyToShoot()
    {
        yield return new WaitForSecondsRealtime(2.0f);
        GameManager.instance.readyToShoot = true;
    }
}
