﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public static UIManager instance;

    public GameObject[] allBallsImg;
    public Sprite enableBallImg;
    public Sprite disableBallImg;

    public GameObject fade;
    public GameObject HomeUI, GameUI;
    public GameObject gameScene;

    public int score;
    public Text textScore;


    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    void Start()
    {
        gameScene.SetActive(false);
    }

    public void UpdateBallIcons()
    {
        int ballcount = GameManager.instance.totalBalls;
        for (int i = 0; i < 5; i++)
        {
            if (i < ballcount)
            {
                allBallsImg[i].GetComponent<Image>().sprite = enableBallImg;
            }
            else
            {
                allBallsImg[i].GetComponent<Image>().sprite = disableBallImg;
            }
        }

    }

    public void B_Start()
    {
        StartCoroutine(StartGameRoutine());
    }

    public void B_Exit()
    {
        Application.Quit();
    }

    IEnumerator StartGameRoutine()
    {
        StartCoroutine(fadeRoutine());
        yield return new WaitForSeconds(0.5f);
        HomeUI.SetActive(false);
        gameScene.SetActive(true);
        GameUI.SetActive(true);
        GameManager.instance.StartGame();
    }

    public void showBlackFade()
    {
        StartCoroutine(fadeRoutine());
    }

    IEnumerator fadeRoutine()
    {
        fade.SetActive(true);
        yield return new WaitForSeconds(1.5f);
        fade.SetActive(false);

    }

    public void UpdateScore()
    {
        score += 1;
        textScore.text = score.ToString();
    }
}
